export default {
	run () {
		return Get_Sections.data.data.filter(
			s => s.help_center_id === (get_help_centers.data.data.find(
				h => h.institutions.find(Boolean)?.id === FAQ_Form.formData.institutions
			)?.id)).map(section => {
			return {
				label: section.title,
				value: section.id
			}
		})
	},
}